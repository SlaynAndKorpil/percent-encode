# Percent-encode

## Installation

You will need the [rust compiler](https://rustup.rs) to compile this.

Then you can compile this project with:
```console
$ cargo build --release
```

and after that run it with:
```console
$ ./target/release/percent-encode
```

## Usage

Encodes URIs with [percent-encoding](https://en.wikipedia.org/wiki/Percent-encoding) or does the reverse.

```text
percent-encode [--help|--reverse|-h|-r] <URI> ...
```

## Examples

```console
┌felix@wheatly:~/Desktop/percent-encode (master)
└$ ./target/release/percent-encode --help
percent-encode [--help|--reverse|-h|-r] <URI> ...

┌felix@wheatly:~/Desktop/percent-encode (master)
└$ ./target/release/percent-encode "https://gitlab.com/SlaynAndKorpil/percent-encode/"
https%3a%2f%2fgitlab.com%2fSlaynAndKorpil%2fpercent-encode%2f
┌felix@wheatly:~/Desktop/percent-encode (master)
└$ ./target/release/percent-encode -r "https%3a%2f%2fgitlab.com%2fSlaynAndKorpil%2fpercent-encode%2f"
https://gitlab.com/SlaynAndKorpil/percent-encode/
┌felix@wheatly:~/Desktop/percent-encode (master)
└$ ./target/release/percent-encode "https://gitlab.com/SlaynAndKorpil/percent-encode/" | xargs ./target/release/percent-encode -r
https://gitlab.com/SlaynAndKorpil/percent-encode/
```
