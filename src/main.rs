/// RFC 3986 section 2.2 Reserved Characters (January 2005)
const RESERVED_CHARACTERS: &[char] = &[
	'!', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '/', ':', ';', '=', '?', '@',
	'[', ']',
];

const HELP_TEXT: &str = "percent-encode [--help|--reverse|-h|-r] <URI> ...\n";

fn encode(i: &str) -> String {
	let mut res = String::with_capacity(i.len());

	for c in i.chars() {
		// assuming all characters are either in the unreserved or reserved character
		// range as defined by RFC 3986 section 2.2 & 2.3
		if RESERVED_CHARACTERS.contains(&c) {
			res.push('%');
			res.push_str(&format!("{:02x}", c as u8));
		} else {
			res.push(c);
		}
	}

	res
}

enum DecodingState {
	Normal,
	ParseFirst,
	ParseSecond,
}

/// Assumes the input `i` is a valid encoded URI.
fn decode(i: &str) -> String {
	let mut res = String::with_capacity(i.len());

	let mut decoding_state = DecodingState::Normal;
	let mut decoding_value: u8 = 0;

	// When encountering a '%', parse the next two digits as hex representation of the
	// corresponding ASCII characters and add that.
	// Otherwise just add the character.
	for c in i.chars() {
		decoding_state = match decoding_state {
			DecodingState::Normal => {
				if c == '%' {
					DecodingState::ParseFirst
				} else {
					res.push(c);

					DecodingState::Normal
				}
			}
			DecodingState::ParseFirst => {
				// unwrapping as it is assumed the characters are valid data
				decoding_value += (c.to_digit(16).unwrap() as u8) << 4;

				DecodingState::ParseSecond
			}
			DecodingState::ParseSecond => {
				// unwrapping as it is assumed the characters are valid data
				decoding_value += c.to_digit(16).unwrap() as u8;
				res.push(decoding_value as char);

				decoding_value = 0;
				DecodingState::Normal
			}
		}
	}

	res
}

fn main() {
	let mut reverse = false;
	let mut results: Vec<String> = Vec::new();

	for arg in std::env::args().skip(1) {
		if let Some(stripped) = arg.strip_prefix('-') {
			match stripped {
				"r" | "-reverse" => reverse = true,
				"h" | "-help" => {
					println!("{}", HELP_TEXT);
					std::process::exit(0);
				}
				_ => {
					eprintln!("Found unexpected argument: {}\n\n{}", arg, HELP_TEXT);
					std::process::exit(1);
				}
			}
		} else if !reverse {
			results.push(encode(&arg));
		} else {
			results.push(decode(&arg));

			reverse = false;
		}
	}

	for result in results {
		println!("{}", result);
	}
}
